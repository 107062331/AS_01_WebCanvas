var _board = document.getElementById('CanvasBoard');
var _picker = document.getElementById('ColorPicker');

var ctx_b = _board.getContext('2d');
var ctx_p = _picker.getContext('2d');

var nowMode;
var CurImg;
var RefeBoardMouse;
var PickerImg;
var undoImgList;
var redoImgList;

function DrawPicker(){
  var p_width = _picker.width;
  var p_height = _picker.height;
  var grd = ctx_p.createLinearGradient(0, 0, 0, p_height);
  grd.addColorStop(0, 'rgba(255, 0, 0, 1)');
  grd.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
  grd.addColorStop(0.34, 'rgba(0, 255, 0, 1)');
  grd.addColorStop(0.51, 'rgba(0, 255, 255, 1)');
  grd.addColorStop(0.68, 'rgba(0, 0, 255, 1)');
  grd.addColorStop(0.85, 'rgba(255, 0, 255, 1)');
  grd.addColorStop(1, 'rgba(255, 0, 0, 1)');
  ctx_p.fillStyle = grd;
  ctx_p.fillRect(0, 0, p_width, p_height);
}

function init(){
  ctx_b.lineJoin = 'round';
  ctx_b.lineCap = 'round';
  ctx_b.lineWidth = '5px';
  ctx_b.strokeStyle = 'black';
  nowMode = 0;
  DrawPicker();
  PickerImg = new Image();
  undoImgList = [];
  redoImgList = [];
}

function GetMousePos(evt){
  return {
    x: evt.clientX,
    y: evt.clientY
  }
};

function BoardMousePos(canvas, evt){
  var MousePos = GetMousePos(evt);
  return {
    x: MousePos.x - canvas.getBoundingClientRect().left,
    y: MousePos.y - canvas.getBoundingClientRect().top
  }
};

function SaveCurBoard(){
  CurImg = new Image();
  CurImg.src = _board.toDataURL();
};

function ClearCurBoard(){
  ctx_b.clearRect(0, 0, _board.width, _board.height);
};

function DrawBoard(newImg, tarBoard){
  ctx_b.drawImage(newImg, 0, 0, newImg.width, newImg.height, 0, 0, tarBoard.width, tarBoard.height);
}

function BoardBrushSize(newSize){
  ctx_b.lineWidth = newSize;
  document.getElementById('BrushSizeMonitor').innerText = newSize + 'px';
}

function BoardBrushColor(r ,g ,b){
  ctx_b.strokeStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
  document.getElementById('ColorMonitor').style.backgroundColor = 'rgb(' + r + ',' + g + ',' + b + ')';
  document.getElementById('RMonitor').innerText = 'R: ' + r;
  document.getElementById('GMonitor').innerText = 'G: ' + g;
  document.getElementById('BMonitor').innerText = 'B: ' + b;
}

function StoreImg(List, redoKeep){
  redoKeep = redoKeep || false;
  if(!redoKeep){
    redoImgList = [];
  }
  List.push(_board.toDataURL()); 
}

function TakeImg(popL, pushL){
  if(popL.length > 0){
    StoreImg(pushL, true);
    var newImg = popL.pop();
    var img = new Image();
    img.onload = function(){
      ClearCurBoard();
      DrawBoard(img, _board)
    }
    img.src = newImg;
  }
}

function Undomode(){
  TakeImg(undoImgList, redoImgList);
}

function Redomode(){
  TakeImg(redoImgList, undoImgList);
}

function ModeSelect(newMode){
  if(newMode == 1){
    _board.style.cursor = 'n-resize';
    ctx_b.globalCompositeOperation = 'source-over';
  }
  else if(newMode == 2){
    _board.style.cursor = 'ne-resize';
    ctx_b.globalCompositeOperation = 'destination-out';
  }
  else if(newMode == 3){
    _board.style.cursor = 'e-resize';
    ctx_b.globalCompositeOperation = 'source-over';
  }
  else if(newMode == 4){
    _board.style.cursor = 'nw-resize';
    ctx_b.globalCompositeOperation = 'source-over';
  }
  else if(newMode == 5){
    _board.style.cursor = 'ns-resize';
    ctx_b.globalCompositeOperation = 'source-over';
  }
  else if(newMode == 6){
    _board.style.cursor = 'default';
    Undomode();
  }
  else if(newMode == 7){
    _board.style.cursor = 'default';
    Redomode();
  }
  else if(newMode == 8){
    _board.style.cursor = 'default';
    ClearCurBoard();
  }
  nowMode = newMode;
}

function BoardMouseMove(evt){
  var mousePos = BoardMousePos(_board, evt);
  if(nowMode == 1){
    ctx_b.lineTo(mousePos.x, mousePos.y);
    ctx_b.stroke();
  }
  else if(nowMode == 2){
    ctx_b.lineTo(mousePos.x, mousePos.y);
    ctx_b.stroke();
  }
  else if(nowMode == 3){
    ClearCurBoard();
    DrawBoard(CurImg, _board);
    ctx_b.beginPath();
    ctx_b.rect(RefeBoardMouse.x, RefeBoardMouse.y, mousePos.x - RefeBoardMouse.x, mousePos.y - RefeBoardMouse.y);
    ctx_b.stroke();
  }
  else if(nowMode == 4){
    ClearCurBoard();
    DrawBoard(CurImg, _board);
    var diameter = Math.sqrt(((RefeBoardMouse.x - mousePos.x) * (RefeBoardMouse.x - mousePos.x)) + ((RefeBoardMouse.y - mousePos.y) * (RefeBoardMouse.y - mousePos.y)));
    ctx_b.beginPath();
    ctx_b.arc((RefeBoardMouse.x + mousePos.x)/2, (RefeBoardMouse.y + mousePos.y)/2, diameter/2, 0, 2 * Math.PI);
    ctx_b.stroke();
  }
  else if(nowMode == 5){
    ClearCurBoard();
    DrawBoard(CurImg, _board);
    ctx_b.beginPath();
    ctx_b.moveTo(RefeBoardMouse.x, RefeBoardMouse.y);
    ctx_b.lineTo(mousePos.x, mousePos.y);
    ctx_b.lineTo(mousePos.x, RefeBoardMouse.y);
    ctx_b.closePath();
    ctx_b.stroke();
  }
}

_picker.addEventListener('mousedown', function(evt){
  var mousePos = BoardMousePos(_picker, evt);
  var img_data = ctx_p.getImageData(mousePos.x, mousePos.y, 1, 1).data;
  var rgb = img_data[0] + ',' + img_data[1] + ',' + img_data[2];
  BoardBrushColor(img_data[0], img_data[1], img_data[2]);
});

_board.addEventListener('mousedown', function(evt){
  var mousePos = BoardMousePos(_board, evt);
  if(nowMode == 1){
    ctx_b.beginPath();
    ctx_b.moveTo(mousePos.x, mousePos.y);
  }
  if(nowMode == 2){
    ctx_b.beginPath();
    ctx_b.moveTo(mousePos.x, mousePos.y);
  }
  if(nowMode == 3){
    SaveCurBoard();
    RefeBoardMouse = mousePos;
  }
  if(nowMode == 4){
    SaveCurBoard();
    RefeBoardMouse = mousePos;
  }
  if(nowMode == 5){
    SaveCurBoard();
    RefeBoardMouse = mousePos;
  }

  StoreImg(undoImgList, false);
  evt.preventDefault();
  _board.addEventListener('mousemove', BoardMouseMove);
});

_board.addEventListener('mouseup', function(evt){
  _board.removeEventListener('mousemove', BoardMouseMove);
});
