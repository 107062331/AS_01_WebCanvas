# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
    鉛筆：透過滑鼠可於畫板上進行塗鴉。
          當按下滑鼠時進行滑動，於結束時再將滑鼠放開，即完成繪畫。
          旁邊面板可進行筆刷大小以及筆刷顏色的改動。
          下圖為鉛筆圖示。
![img](./pencil.jpg)
###
          下圖為鉛筆範例。其中表示各個的筆刷大小跟顏色。
![img](./test_pen.jpg)
###
    擦布：可清除畫板上不需要的地方。
          使用方式與鉛筆同。
          旁邊面板可進行擦布大小的改動。
          下圖為擦布圖示。
![img](./eraser.png)
###
          下圖為擦布範例。其中透過鉛筆來表示各個的擦布大小。
![img](./test_eraser.jpg)
###
    矩形繪圖：在面板上畫出一個空心的矩形。
              先在面板上固定一點，其後再進行拖動預覽效果，當結束時放開滑鼠以完成矩形繪圖。
              旁邊面板可進行矩形厚度以及矩形顏色的改動。
              下圖為矩形繪圖圖示。
![img](./rectangle.png)
###
              下圖為矩形繪圖範例。其中表示各個的矩形厚度跟顏色。
![img](./test_rect.jpg)
###
    圓形繪圖：在面板上畫出一個空心的圓形。
              使用方式與矩形繪圖同。
              旁邊面板可進行圓形厚度以及圓形顏色的改動。
              下圖為圓形繪圖圖示。
![img](./circle.png)
###
              下圖為圓形繪圖範例。其中表示各個的圓形厚度跟顏色。
![img](./test_circle.jpg)
###
    直角三角形繪圖：在面板上畫出一個空心的直角三角形。
                    使用方式與矩形繪圖同。
                    旁邊面板可進行直角三角形厚度以及直角三角形顏色的改動。
                    下圖為直角三角形繪圖圖示。
![img](./triangle.png)
###
                    下圖為直角三角形繪圖範例。其中表示各個的直角三角形厚度跟顏色。
![img](./test_trian.jpg)
###
    復原：可恢復至上一個動作。
          直接點擊即可。
          不可與擦布共用。
          下圖為復原圖示。
![img](./undo.jpeg)
###
          此為範例比較圖。
![img](./test_undosamp.jpg)
###
          下圖為復原範例。其中表示復原至2的圖示。
![img](./test_undo.jpg)
###
    取消復原：原本使用復原的動作，可藉由取消復原到下一個動作。
              使用方式與復原同。
              不可與擦布共用。
              下圖為取消復原圖示。
![img](./redo.png)
###
              下圖為復原範例。其中表示取消復原至3的圖示。
![img](./test_redo.jpg)
###
    清除畫板：會清除面板上的所有圖案。
              直接點擊即可。
              可藉由復原來恢復圖案。
              下圖為清除畫板圖示。
![img](./clear.png)
###
              下圖為復原範例。實在不知道要怎麼表示。
![img](./test_clear.jpg)
### Gitlab page link
   https://107062331.gitlab.io/AS_01_WebCanvas
### Others (Optional)
![img](./other.jpg)
<style>
table th{
    width: 100%;
}
</style>